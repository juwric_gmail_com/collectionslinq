﻿using CollectionsLINQ.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CollectionsLINQ
{
    public class MappingService
    {
        private HttpClient _client;
        List<Project> projects = new List<Project>();
        List<Models.Task> tasks = new List<Models.Task>();
        List<Team> teams = new List<Team>();
        List<User> users = new List<User>();

        public MappingService()
        {
            _client = new HttpClient();
        }

        public async System.Threading.Tasks.Task Init()
        {
            teams = await GetTeams();
            projects = await GetProjects();
            users = await GetUsers();
            tasks = await GetTasks();
        }

        public IEnumerable<Project> GetProjectList()
        {
            var projectsMapping = from project in projects
                                  join user in users on project.AuthorId equals user.Id
                                  join team in teams on project.TeamId equals team.Id
                                  select new Project
                                  {
                                      Name = project.Name,
                                      Description = project.Description,
                                      Deadline = project.Deadline,
                                      CreatedAt = project.CreatedAt,
                                      AuthorId = project.AuthorId,
                                      Author = user,
                                      TeamId = project.TeamId,
                                      Team = team
                                  };

            var tasksMapping =
               from task in tasks
               join user in users on task.PerformerId equals user.Id
               select new Models.Task
               {
                   Id = task.Id,
                   ProjectId = task.ProjectId,
                   PerformerId = task.PerformerId,
                   Performer = user,
                   Name = task.Name,
                   Description = task.Description,
                   State = task.State,
                   CreatedAt = task.CreatedAt,
                   FinishedAt = task.FinishedAt
               };

            var result = projectsMapping.GroupJoin(tasksMapping,
                project => project.Id,
                task => task.ProjectId,
                (project, task) => new Project
                {
                    Name = project.Name,
                    Description = project.Description,
                    Deadline = project.Deadline,
                    CreatedAt = project.CreatedAt,
                    AuthorId = project.AuthorId,
                    Author = project.Author,
                    TeamId = project.TeamId,
                    Team = project.Team,
                    Tasks = task.Select(t => t)
                });

            return result;
        }

        public List<dynamic> Request4()
        {
            int year = DateTime.Now.Year;
            var result = teams.GroupJoin(
                users,
                team => team.Id,
                user => user.TeamId,
                (team, user) => new
                {
                    id = team.Id,
                    name = team.Name,
                    users = user
                    .OrderByDescending(u => u.RegisteredAt)
                    .Where(u => (year - u.BirthDay.Year) > 10)
                });

            return result.ToList<dynamic>();
        }

        public List<dynamic> Request5()
        {
            var result = users.OrderBy(u => u.FirstName).GroupJoin(
                tasks,
                user => user.Id,
                task => task.PerformerId,
                (user, task) => new
                {
                    first_name = user.FirstName,
                    tasks = task.OrderByDescending(t => t.Name.Length)
                });

            return result.ToList<dynamic>();
        }

        public List<dynamic> Request6(int userId = 34)
        {
            var result = users.Where(u => u.Id == userId).GroupJoin(projects,
                u => u.Id,
                p => p.AuthorId,
                (u, p) => new
                {
                    user = u,
                    lastProject = p.OrderBy(lp => lp.CreatedAt).First(),
                }).GroupJoin(tasks,
                    prev => prev.user.Id,
                    t => t.PerformerId,
                    (prev, t) => new
                    {
                        user = prev.user,
                        lastProject = prev.lastProject,
                        totalUnFinishedOrCanceled = t.Where(t =>
                            t.State == TaskState.UnFinished || t.State == TaskState.Canceled),
                        longestTask = t.OrderByDescending(t => t.FinishedAt - t.CreatedAt).First()
                    });

            return result.ToList<dynamic>();
        }

        public List<dynamic> Request7()
        {
            var result = projects.Select(p => new
            {
                project = p,
                longTask = p.Tasks.OrderByDescending(t => t.Description.Length).First(),
                shortTask = p.Tasks.OrderBy(t => t.Name.Length).First(),
            });

            return null;
        }

        private async Task<List<Models.User>> GetUsers()
        {
            var users = await _client.GetStringAsync("https://bsa-dotnet.azurewebsites.net/api/Users");
            return JsonConvert.DeserializeObject<List<User>>(users);
        }

        private async Task<List<Team>> GetTeams()
        {
            var teams = await _client.GetStringAsync("https://bsa-dotnet.azurewebsites.net/api/Teams");
            return JsonConvert.DeserializeObject<List<Team>>(teams);
        }

        private async Task<List<Models.Task>> GetTasks()
        {
            var teams = await _client.GetStringAsync("https://bsa-dotnet.azurewebsites.net/api/Tasks");
            return JsonConvert.DeserializeObject<List<Models.Task>>(teams);
        }

        private async Task<List<Project>> GetProjects()
        {
            var teams = await _client.GetStringAsync("https://bsa-dotnet.azurewebsites.net/api/Projects");
            return JsonConvert.DeserializeObject<List<Project>>(teams);
        }
    }
}
