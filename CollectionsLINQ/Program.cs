﻿using CollectionsLINQ.Models;
using System.Collections.Generic;
using System.Linq;

namespace CollectionsLINQ
{
    internal class Program
    {
        static IEnumerable<Project> list;

        public static void Main(string[] args)
        {
            MappingService service = new MappingService();
            service.Init().Wait();
            list = service.GetProjectList();

            var req1 = Request1();
            var req2 = Request2();
            var req3 = Request3();
            var req4 = service.Request4();
            var req5 = service.Request5();
            var req6 = service.Request6();
            var req7 = service.Request7();
        }

        public static Dictionary<Project, int> Request1(int userId = 29)
        {
            return list.Where(q => q.AuthorId == userId)
                .ToDictionary(p => p, t => t.Tasks.Count());
        }

        public static List<Task> Request2(int userId = 34)
        {
            var result = list.SelectMany(p => p.Tasks)
                .Where(t => t.PerformerId == userId && t.Name.Length < 45);

            return result.ToList();
        }

        public static List<dynamic> Request3(int userId = 34)
        {
            var result = list.SelectMany(p => p.Tasks)
                .Where(t => t.PerformerId == userId && t.State == TaskState.Finished && t.FinishedAt?.Year == 2021)
                .Select(t => new { id = t.Id, name = t.Name });

            return result.ToList<dynamic>();
        }
    }
}
